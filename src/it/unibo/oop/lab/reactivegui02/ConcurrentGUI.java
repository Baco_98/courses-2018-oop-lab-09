package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class ConcurrentGUI extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    
    private final JLabel display = new JLabel();
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    private final JButton stop = new JButton("stop");
    
    private volatile boolean stopWork = false;
    
    public ConcurrentGUI(){
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.getContentPane().add(panel);
        this.setVisible(true);
        
        final Agent agent = new Agent();
        new Thread(agent).start();
        
        stop.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                stopCounting();                
            }
        });
        
        up.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                agent.increment();                
            }
        });
        
        down.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                agent.decrement();                
            }
        });
        
    }
    
    public void stopCounting() {
        this.stopWork = true;
        up.setEnabled(false);
        down.setEnabled(false);
        stop.setEnabled(false);
    }
    
    public class Agent implements Runnable{
        
        private Integer counter = 0;
        private volatile boolean incrementing = true;
        
        
        @Override
        public void run() {
            while(!stopWork) {
                try {
                    SwingUtilities.invokeAndWait(new Runnable(){
                        @Override
                        public void run() {
                            ConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter)); 
                        }
                    });
                    if(this.incrementing) {
                        this.counter++;
                    }else {
                        this.counter--;
                    }
                    Thread.sleep(100);
                }catch (InvocationTargetException | InterruptedException ex) {
                    break;
                }
            }
            
        }
        
        
        public void increment() {
            this.incrementing=true;
        }
        
        public void decrement() {
            this.incrementing=false;
        }
    }
}

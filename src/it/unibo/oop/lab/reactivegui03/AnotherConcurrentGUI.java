package it.unibo.oop.lab.reactivegui03;

import it.unibo.oop.lab.reactivegui02.ConcurrentGUI;

public class AnotherConcurrentGUI extends ConcurrentGUI {

    /**
     * 
     */
    private static final long serialVersionUID = 2L;

    public AnotherConcurrentGUI() {
        super();
        
        final Agent2 agent2 = new Agent2();
        new Thread(agent2).start();
    }
    
    private class Agent2 implements Runnable{

        @Override
        public void run() {
            try {
                Thread.sleep(5000);
                stopCounting();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
    }

}

package it.unibo.oop.lab.workers02;

import java.util.ArrayList;
import java.util.List;

public class MultiThreadedSumMatrix implements SumMatrix {

	private final int nThreads;

	public MultiThreadedSumMatrix(final int nThreads) {
		super();
		this.nThreads = nThreads;
	}

	private static class Worker extends Thread {

		private final double[][] list;
        private final int startpos;
        private final int nelem;
        private double res;

		Worker(final double[][] list, final int startpos, final int nelem) {
			super();
			this.list = list;
			this.startpos = startpos;
			this.nelem = nelem;
		}

		@Override
		public void run() {
			System.out.println("Working from line: " + this.startpos + " for "
								+ (this.nelem - 1) + " lines");
			for (int i = this.startpos; i < this.startpos + this.nelem && i < list.length; i++) {
				for (int k = 0; k < list[i].length; k++) {
					this.res += list[i][k];
				}
			}
		}

		public double getResult() {
			return this.res;
		}
	}

	@Override
	public final double sum(final double[][] matrix) {
		final int size = matrix.length % nThreads + matrix.length / nThreads;

		final List<Worker> workers = new ArrayList<>(nThreads);
        for (int start = 0; start < matrix.length; start += size) {
            workers.add(new Worker(matrix, start, size));
        }
        /*
         * Start them
         */
        for (final Worker w: workers) {
            w.start();
        }
        /*
         * Wait for every one of them to finish. This operation is _way_ better done by
         * using barriers and latches, and the whole operation would be better done with
         * futures.
         */
        double sum = 0;
        for (final Worker w: workers) {
            try {
                w.join();
                sum += w.getResult();
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
        /*
         * Return the sum
         */
        return sum;
	}

}
